import { useState } from "react";
import styles from "./NewsletterForm.module.css";

export const NewsletterForm = () => {
    const [email, setEmail] = useState<string>("");
    const [success, setSuccess] = useState<boolean>(false);
    const handleEmailChange = (e: React.FormEvent<HTMLInputElement>) => {
        setEmail(e.currentTarget.value);
    };
    const handleSubmit = (e: React.FormEvent<HTMLButtonElement>) => {
        e.preventDefault();
        if (!email) {
            setSuccess(false);
            return;
        }
        setSuccess(true);
    };
    return (
        <div className={styles.container}>
            <form className={styles.form}>
                <input name="email" value={email} onChange={handleEmailChange} className={styles.input} />
                <button type="submit" className={styles.button} onClick={handleSubmit}>
                    Subscribe
                </button>
            </form>
            {success && <p className={styles.success}>{email} added to the mailing list!</p>}
        </div>
    );
};
