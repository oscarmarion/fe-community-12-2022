import styles from "./TopNav.module.css";
export const TopNav = () => {
    return (
        <div className={styles.topnav}>
            <a className={styles.link} href="http://localhost:4000">
                Astro
            </a>
            <a className={styles.link} href="http://localhost:4001">
                Next
            </a>
            <a className={styles.link} href="http://localhost:4002">
                React
            </a>
        </div>
    );
};
