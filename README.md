# FE community 12-2022

This is an official npm starter turborepo.

## What's inside?

This turborepo uses [npm](https://www.npmjs.com/) as a package manager. It includes the following packages/apps:

### Apps and Packages

-   `astro`
-   `nextjs`
-   `react`
-   `mock-types`: shared typescript interfaces for the mock API used throughout the monorepo
-   `ui`: a stub React component library shared throughout the monorepo
-   `eslint-config-custom`: `eslint` configurations (includes `eslint-config-next` and `eslint-config-prettier`)
-   `tsconfig`: `tsconfig.json`s used throughout the monorepo

Each package/app is 100% [TypeScript](https://www.typescriptlang.org/).

### Build

To build all apps and packages, run the following command:

```
cd fe-community-12-2022
npm run build
```

### Develop

To develop all apps and packages, run the following command:

```
cd fe-community-12-2022
npm run dev
```

### Preview (production build)

> A build is required before running this command

To preview all apps, run the following command:

```
cd fe-community-12-2022
npm run preview
```
