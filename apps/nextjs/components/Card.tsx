import { Product } from "mock-types";
import Image from "next/image";
import { FC, useEffect, useState } from "react";
import styles from "./Card.module.css";
interface Props {
    product: Product;
    index: number;
}

export const Card: FC<Props> = ({ product, index }) => {
    return (
        <li className={styles.product}>
            <Image
                className={styles.image}
                src={`https://picsum.photos/id/${index}/300/150`}
                alt={product.title}
                width={300}
                height={150}
            />
            <h2>
                {product.title}
                <span>&rarr;</span>
            </h2>
            <p>{product.description}</p>
        </li>
    );
};
