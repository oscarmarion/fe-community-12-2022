// module.exports = {
//     reactStrictMode: true,
//     experimental: {
//         transpilePackages: ["ui"],
//         appDir: true
//     },
//     images: {
//         remotePatterns: [
//             {
//                 protocol: "https",
//                 hostname: "picsum.photos"
//             }
//         ]
//     }
// };

const withTM = require("next-transpile-modules");

const nextConfig = {
    reactStrictMode: true,
    experimental: {
        transpilePackages: ["ui"]
        // appDir: true
    },
    images: {
        remotePatterns: [
            {
                protocol: "https",
                hostname: "picsum.photos"
            }
        ]
    }
};
module.exports = withTM(["ui"])(nextConfig);
