import { Button, NewsletterForm, TopNav } from "ui";
import { Product, Products } from "mock-types";
import { Card } from "../components/Card";
import Head from "next/head";

interface Props {
    products: Product[];
}

export default function Web({ products }: Props) {
    return (
        <>
            <Head>
                <title>NextJS</title>
            </Head>
            <div id="root">
                <TopNav />
                <h1>NextJS</h1>
                <ul>
                    {products?.map((product) => (
                        <Card product={product} index={product.id} />
                    ))}
                </ul>
                <footer>
                    <NewsletterForm />
                </footer>
            </div>
        </>
    );
}

export async function getServerSideProps() {
    // Fetch data from external API
    const res = await fetch(`https://dummyjson.com/products`);
    const json = (await res.json()) as Products;

    // Pass data to the page via props
    return { props: { products: json.products } };
}
