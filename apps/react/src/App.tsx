import { useEffect, useState } from "react";
import "./App.css";
import { Product, Products } from "mock-types";
import { NewsletterForm, TopNav } from "ui";
import { Card } from "./Card";

function App() {
    const [products, setProducts] = useState<Product[]>();
    useEffect(() => {
        fetch("https://dummyjson.com/products")
            .then((response) => response.json())
            .then((json: Products) => {
                setProducts(json.products);
            });
    }, []);
    return (
        <div className="App">
            <TopNav />
            <h1>React</h1>
            <ul>
                {products?.map((product) => (
                    <Card title={product.title} index={product.id} />
                ))}
            </ul>
            <footer>
                <NewsletterForm />
            </footer>
        </div>
    );
}

export default App;
