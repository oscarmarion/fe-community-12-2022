import { Product } from "mock-types";
import { FC, useEffect, useState } from "react";
import styles from "./Card.module.css";
interface Props {
    title: string;
    index: number;
}

export const Card: FC<Props> = ({ title, index }) => {
    const [product, setProduct] = useState<Product>();
    useEffect(() => {
        fetch(`https://dummyjson.com/products/${index}`)
            .then((response) => response.json())
            .then((json: Product) => {
                setProduct(json);
            });
    }, []);
    return (
        <li className={styles.product}>
            <img
                className={styles.image}
                loading="lazy"
                src={`https://picsum.photos/id/${index}/300/150`}
                alt={title}
            />
            <h2>
                {title}
                <span>&rarr;</span>
            </h2>
            <p>{product?.description}</p>
        </li>
    );
};
